fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'GET',
})
.then(response => response.json())
.then(data => {console.log(data)
})	

const array = [
  {
    "userId": 1,
    "id": 1,
    "title": "delectus aut autem",
    "completed": false
  },
  {
    "userId": 1,
    "id": 2,
    "title": "quis ut nam facilis et officia qui",
    "completed": false
  },
  {
    "userId": 2,
    "id": 2,
    "title": "quis ut nam facilis et officia qui",
    "completed": false
  }
  
]


//4
const userArray = array.map(array => {
  if(array.userId == 1)
  console.log(array.title)
  })

//5
fetch('https://jsonplaceholder.typicode.com/todos?userId=1',{
	method: "GET",

})
.then(res => res.json())
.then(data => console.log(data))

//6

fetch('https://jsonplaceholder.typicode.com/todos/199',{
  method: "GET"

})
.then(res => res.json())
.then(data => console.log(`The tilte is ${data.title}, and the status is ${data.completed}`))



//7
fetch('https://jsonplaceholder.typicode.com/todos', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    title: 'New post',
    body: 'Hello World, Hello to all',
    userId: 1
  })
})
.then(response => response.json())
.then(json => console.log(json))


//8
fetch('https://jsonplaceholder.typicode.com/todos/3', {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({

    title: "Updated post",
    body: "Hello again, Hello to all",
    userId: 11
  })
})
.then(res => res.json())
.then(data => console.log(data))


//9

fetch('https://jsonplaceholder.typicode.com/todos/5', {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({

    title: "Updated post",
    description: "I updated this line",
    status: false,
    dateCompleted: '30/03/22',
    userId: 2
  })
})
.then(res => res.json())
.then(data => console.log(data))


//10

fetch('https://jsonplaceholder.typicode.com/todos/10', {
  method: 'PATCH',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    title: "Updated post using Patch method",
    completed: false
  })
})
.then(res => res.json())
.then(data => console.log(data))


//11

fetch('https://jsonplaceholder.typicode.com/todos/8', {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({

    title: "Updated post",
    description: "I updated this line",
    status: "complete",
    dateComplete: "20/03/22"
    
  })
})
.then(res => res.json())
.then(data => console.log(data))


//12
fetch('https://jsonplaceholder.typicode.com/posts/199', {
  method: 'DELETE'
})
.then(res => res.json())
.then(data => console.log(data))














